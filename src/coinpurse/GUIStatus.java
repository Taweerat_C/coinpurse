package coinpurse;

import java.awt.Font;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

/**
 * Graphical User Interface for show the status of purse.
 * @author Taweerat Chaiman 5710546259
 * @version 24.02.2015
 */
public class GUIStatus implements java.util.Observer{
	/**Class attributes.*/
	private JFrame frame;
	private JLabel lb_status;
	private JPanel panel;
	private JProgressBar progressBar;
	private java.util.Observable purse;
	
	/**
	 * Constructor and initial the attributes.
	 * Show GUI.
	 * @param subject as Observable that this class have to listen to.
	 */
	public GUIStatus(java.util.Observable subject){
		this.purse = subject;
		initComponent();
	}
	
	/**
	 * Initial component of GUI.
	 */
	public void initComponent(){
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		panel = new JPanel();
		panel.setLayout(new BoxLayout(panel,BoxLayout.Y_AXIS));
		if(this.purse instanceof Purse){
			Purse purse = (Purse)this.purse;
			if(!purse.isFull()){
				lb_status = new JLabel("Available " + purse.getCapacity() + " slots");
			}else{
				lb_status = new JLabel("Unavailable purse");
			}
		}else
			lb_status = new JLabel("Unavailable purse");
		Font font = lb_status.getFont();
		lb_status.setFont(new Font(font.getName(),Font.BOLD,35));
		progressBar = new JProgressBar();
		progressBar.setMinimum(0);
		progressBar.setStringPainted(true);
		panel.add(lb_status);
		panel.add(progressBar);
		frame.add(panel);
		frame.pack();
		frame.setVisible(true);
		
	}
	
	/**
	 * update information when observer is updated.
	 * @param subject as Observable that listening to
	 * @param info as information
	 */
	public void update(java.util.Observable subject,Object info){
		if(subject instanceof Purse){
			Purse purse = (Purse)subject;
			if(!purse.isFull())
				lb_status.setText("Available " + (purse.getCapacity()-purse.count()) + " slots");
			else
				lb_status.setText("Full");
			progressBar.setMaximum(purse.getCapacity());
			progressBar.setValue(purse.count());
			frame.pack();
		}
	}
}
