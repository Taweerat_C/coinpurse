package coinpurse.factory;

import java.util.ResourceBundle;

import coinpurse.Valuable;

public abstract class MoneyFactory {
	private static MoneyFactory moneyFactory=null;
	public static MoneyFactory getInstance() throws InstantiationException, IllegalAccessException, ClassNotFoundException{
		if(moneyFactory == null){
			ResourceBundle bundle = ResourceBundle.getBundle("coinpurse.purse");
	    	String factoryClass = bundle.getString("moneyfactory");
	    	moneyFactory = (MoneyFactory) Class.forName(factoryClass).newInstance();
		}
		return moneyFactory;
	}
	
	abstract Valuable createMoney( double value );
	
	public Valuable createMoney(String valueStr){
		double value = Double.parseDouble(valueStr);
		return moneyFactory.createMoney(value);
	}
}
