package coinpurse.factory;

import java.util.ArrayList;
import java.util.List;

import coinpurse.BankNote;
import coinpurse.Coin;
import coinpurse.Valuable;

public class MalaysiaMoneyFactory extends MoneyFactory{
	private final String BANKNOTE_CURR = "Ringgit";
	private final String COIN_CURR = "Sen";
	private List<Double> senList = new ArrayList<Double>();
	public MalaysiaMoneyFactory(){
		senList.add(0.05);
		senList.add(0.10);
		senList.add(0.20);
		senList.add(0.50);
	}
	@Override
	Valuable createMoney(double value) {
		int forCase = (int)value;
		switch(forCase){
		case 0 :
			if(senList.contains(value))
				return new Coin(value*100,COIN_CURR);
			break;
		case 1: case 2: case 5: case 10: case 20: case 100:
			return new BankNote(value,BANKNOTE_CURR);
		}
		throw new IllegalArgumentException();
	}
	
}
