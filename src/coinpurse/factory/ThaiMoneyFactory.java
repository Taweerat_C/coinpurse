package coinpurse.factory;

import coinpurse.BankNote;
import coinpurse.Coin;
import coinpurse.Valuable;


public class ThaiMoneyFactory extends MoneyFactory{
	private final String CURRENCY = "Bath";
	
	public ThaiMoneyFactory(){
		
	}
	public Valuable createMoney( double value ){
		int forCase = (int)value;
		switch(forCase){
		case 1 : case 2 :  case 5 : case 10 :
			return new Coin(value,CURRENCY);
		case 20 : case 50 : case 100 : case 500 : case 1000 : 
			return new BankNote(value,CURRENCY);
		}
		throw new IllegalArgumentException();
	}
}
