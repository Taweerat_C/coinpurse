package coinpurse;
/**
 * A coin with a monetary value.
 * You can't change the value of a coin.
 * @author Taweerat Chaiman 5710546259
 * @version 10.02.2015
 */
public class Coin extends AbstractValuable{

    /** Value of the coin. */
    double value;
    
    /** 
     * Constructor for a new coin. 
     * @param value is the value for the coin
     */
    public Coin( double value , String currency) {
    	super(currency);
    	
    	//declare initial value of current value
        this.value = value;
    }

    /**
     * Get current value.
     * @return value of this coin
     */
    public double getValue(){
    	return this.value;
    }
    
    /**
     * Get String of this coin.
     * @return "-value- Baht"
     */
    public String toString(){
    	String currency = super.getCurrency();
    	return String.format("%s-%s Coin", value, currency);
    }
   
}
