package coinpurse;
/**
 * BankNote class.
 * @author Taweerat Chaiman 5710546259
 * @version 10.02.2015
 */
public class BankNote extends AbstractValuable{
	private static long nextSerialNumber = 1000000;
	private long currentSerial;
	private double value;
	/**
	 * Constructor of BankNote.
	 * Initial all attribute.
	 * @param value as initial value.
	 */
	public BankNote(double value,String currency){
		super(currency);
		this.value = value;
		this.currentSerial = BankNote.getNextSerialNumber();
		BankNote.nextSerialNumber++;
	}
	
	/**
	 * Get value of this BankNote.
	 * @return double as value
	 */
	public double getValue(){
		return this.value;
	}
	
	/**
	 * Get nextSerialNumber.
	 * @return long as nextSerialNumber
	 */
	public static long getNextSerialNumber(){
		return BankNote.nextSerialNumber;
	}
	 /**
     * Get information of this BankNote.
     * @return String of information
     */
    public String toString(){
    	String currency = super.getCurrency();
    	return String.format("%.0f-%s Banknote [%d]", this.getValue(), currency, this.currentSerial);
    }
    
}
