package coinpurse;
	/**
	 * Coupon class.
	 * @author Taweerat Chaiman 5710546259
	 * @version 10.02.2015
	 */
public class Coupon extends AbstractValuable{
	private String color;

	enum CouponType {
		RED(100), BLUE(50), GREEN(20);
		public final double value;
		CouponType(double value) {
			this.value = value;
		}
	}
	/**
	 * Constructor of Coupon.
	 * @param color as coupon color
	 */
	public Coupon(String color, String currency) {
		super(currency);
		this.color = color.toUpperCase();
	}

	/**
	 * @return Value of coupon as double
	 */
	public double getValue() {
		return CouponType.valueOf(this.color).value;
	}

	/**
	 * Get information of coupon.
	 * @return value of coupon
	 */
	public String toString(){
		return this.color + " Coupon";
	}
	
	
}
