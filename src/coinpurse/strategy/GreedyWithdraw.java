package coinpurse.strategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import coinpurse.Valuable;
/**
 * Withdraw the money with greedy algorithm.
 * @author Taweerat Chaiman 5710546259
 *
 */
public class GreedyWithdraw implements WithdrawStrategy {

	/**
	 * @see WithdrawStrategy#withdraw(double, List)
	 * @return collection of withdrawn money.
	 * @param amount as Money that want to withdraw.
	 * @param valuables as total money is purse. 
	 */
	public Valuable[] withdraw(double amount, List<Valuable> valuables) {

		// if ( ??? ) return ???;

		/*
		 * One solution is to start from the most valuable valuable in the purse
		 * and take any valuable that maybe used for withdraw. Since you don't
		 * know if withdraw is going to succeed, don't actually withdraw the
		 * valuables from the purse yet. Instead, create a temporary list. Each
		 * time you see a valuable that you want to withdraw, add it to the
		 * temporary list and deduct the value from amount. (This is called a
		 * "Greedy Algorithm".) Or, if you don't like changing the amount
		 * parameter, use a local total to keep track of amount withdrawn so
		 * far.
		 * 
		 * If amount is reduced to zero (or tempTotal == amount), then you are
		 * done. Now you can withdraw the valuables from the purse. NOTE: Don't
		 * use list.removeAll(templist) for this becuase removeAll removes *all*
		 * valuables from list that are equal (using valuable.equals) to
		 * something in templist. Instead, use a loop over templist and remove
		 * valuables one-by-one.
		 */

		// Did we get the full amount?

		// Create new ArrayList that duplicated from valuables
		List<Valuable> temp1 = new ArrayList<Valuable>();
		for (int i = 0; i < valuables.size(); i++)
			temp1.add(valuables.get(i));
		// sort temp1 using the valuable's compareTo method
		Collections.sort(temp1);
		List<Valuable> temp2 = new ArrayList<Valuable>();
		for (int i = temp1.size() - 1; i >= 0; i--) {
			// can withdraw if amount >= current value (start from maximum
			// to minimum)
			if (temp1.get(i).getValue() <= amount) {
				temp2.add(temp1.get(i));
				// Decrease amount by minus withdrawn value
				amount -= temp1.get(i).getValue();
			}
		}

		// If withdrawn unsuccessful then return null
		if (amount != 0) {
			return null;
		}

		// Create new array of valuable and return
		Valuable[] valuableTemp = new Valuable[temp2.size()];
		temp2.toArray(valuableTemp);
		if (valuableTemp.length != 0)
			return valuableTemp;

		return null;

	}

}
