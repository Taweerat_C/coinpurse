package coinpurse.strategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import coinpurse.Valuable;
/**
 * Withdraw by recursion.
 * @author Taweerat Chaiman 5710546259
 * @version 19.02.2015
 */
public class RecursiveWithdraw implements WithdrawStrategy {
	/**
	 * Recursion process to find the possibilities that can withdraw the money from purse.
	 * call HELPER METHOD.
	 * @param amount as money that want to withdraw.
	 * @param list as collection of valuable in purse.
	 * @return collection of withdrawn valuable.
	 */
	public Valuable[] withdraw(double amount, List<Valuable> list) {
		/** Check list is empty or not */
		if (list.size() == 0)
			return null;
		/** Clone the list to new list for sorting */
		List<Valuable> listTemp = new ArrayList<Valuable>();
		for (int i = 0; i < list.size(); i++)
			listTemp.add(list.get(i));
		Collections.sort(listTemp);

		/** Recursion process. */
		Valuable[] value = withdraw(amount, listTemp, new ArrayList<Valuable>());
		if(value == null)
			return withdraw(amount,listTemp.subList(0, listTemp.size()-1));
		return value;
	}

	/**
	 * HELPER METHOD.
	 * @param amount as money that want to withdraw.
	 * @param list as collection of valuable in purse.
	 * @param correct as collection of withdrawn valuable.
	 * @return collection of withdrawn valuable.
	 */
	public Valuable[] withdraw(double amount, List<Valuable> list,List<Valuable> correct) {
		if (list.size() == 0 && amount != 0)
			return null;
		if (amount == 0) {
			Valuable[] value = new Valuable[correct.size()];
			correct.toArray(value);
			return value;
		} else if (amount < list.get(0).getValue() && correct.size() == 0)
			return null;
		else if (amount >= list.get(0).getValue()) {
			correct.add(list.get(list.size() - 1));
			amount -= correct.get(correct.size() - 1).getValue();
			return withdraw(amount, list.subList(0, list.size() - 1), correct);
		} else {
			amount += correct.remove(correct.size() - 1).getValue();
			return withdraw(amount, list.subList(0, list.size()), correct);
		}
	}

}
