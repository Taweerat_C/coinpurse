package coinpurse.strategy;

import java.util.List;

import coinpurse.Valuable;

/**
 * Strategy pattern to allow purse change withdrawn behavior.
 * @author Taweerat Chaiman 5710546259
 *
 */
public interface WithdrawStrategy {
	/**
	 * Find elements in the list of Valuable to withdraw such that
	 * the total amount of the element is amount.
	 * This method does not remove any elements from the valuable list.
	 * @param amount is the amount to withdraw, must be greater than zero.
	 * @param valuables as collection of money.
	 * @return collection of withdrawn.
	 */
	Valuable[] withdraw(double amount,List<Valuable> valuables);
}
