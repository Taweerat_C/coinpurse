package coinpurse.strategy;

import java.util.List;

import coinpurse.Valuable;

public class OverWithdraw implements WithdrawStrategy{

	@Override
	public Valuable[] withdraw(double amount, List<Valuable> valuables) {
		RecursiveWithdraw recursive = new RecursiveWithdraw();
		Valuable[] withdrawn = recursive.withdraw(++amount, valuables);
		if(withdrawn == null)
			return recursive.withdraw(++amount, valuables);
		return withdrawn;
	}

}
