 package coinpurse;

import java.util.ResourceBundle;

import coinpurse.factory.MalaysiaMoneyFactory;
import coinpurse.factory.MoneyFactory;
import coinpurse.strategy.RecursiveWithdraw;

/**
 * A main class to create objects and connect objects together.
 * The user interface needs a reference to coin purse.
 */
public class Main {

    /**
     * @param args not used
     * @throws ClassNotFoundException 
     * @throws IllegalAccessException 
     * @throws InstantiationException 
     */
    public static void main( String[] args ) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
    	MoneyFactory factory = MoneyFactory.getInstance();
    	Valuable m = factory.createMoney("0.05");
    	System.out.println(m.toString());
//    	//Create purse with capacity 10
//    	Purse purse = new Purse(10);
//    	GUIStatus statusObserver = new GUIStatus(purse);
//    	GUIBalance balanceObserver = new GUIBalance();
//    	purse.addObserver(statusObserver);
//    	purse.addObserver(balanceObserver);
//    	purse.setWithdrawStrategy(new RecursiveWithdraw());
//    	
//    	//Create diglog for deposit or withdraw
//    	ConsoleDialog s = new ConsoleDialog(purse);
//    	s.run();
    }
}
