package coinpurse;

/**
 * Abstract class for money classes.
 * @author Taweerat Chaiman 5710546259
 *
 */
public abstract class AbstractValuable implements Valuable{
	
	/**Currency*/
	private String currency;
	
	/**
	 * Constructor accept current.
	 * @param currency
	 */
	public AbstractValuable(String currency){
		this.currency = currency;
	}
	
	/**
     * Check for same coin's value.
     * @return true if value of this coin and other coin is same
     * @param other is an object that compare to this object
     */
    public boolean equals(Object other){
    	//false if other is null
    	if(other==null) return false;
    	
    	if(other.getClass() == this.getClass()){
    		Valuable temp = (Valuable)other;
    		return this.getValue() == temp.getValue();
    	}
    	return false;
    		
    }
    
    /**
     * to compare valuable which one is come first or last.
     * use to sort the value.
     * @param valuable as Valuable
     * @return negitive if this value is less than valuable
     * 			positive if this value is greater than valuable
     * 			zero if this value is equals valuable
     */
    public int compareTo(Valuable valuable){
    	if(this.getValue() < valuable.getValue())
    		return -1;
    	else if(this.getValue() > valuable.getValue())
    		return 1;
    	else 
    		return 0;
    }
    
    public String getCurrency(){
    	return this.currency;
    }
}
