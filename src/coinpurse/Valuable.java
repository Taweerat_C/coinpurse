package coinpurse;
/**
 * Interface for get value of Valuable.
 * @author Taweerat Chaiman 5710546259
 * @version 10.02.2015
 *
 */
public interface Valuable extends Comparable<Valuable>{
	/**abstract method.
	 * @return double as 
	 */
	double getValue();
}
