package coinpurse;

/**
 * Show update of purse in console.
 * @author Taweerat Chaiman 5710546259
 * @version 24.02.2015
 */
public class PurseObserver implements java.util.Observer{
	
	/**
	 * Show update information of purse when purse is updated.
	 * @param subject as observable that this observer is listening to.
	 * @param info as information.
	 */
	public void update(java.util.Observable subject,Object info){
		if(subject instanceof Purse){
			Purse purse = (Purse)subject;
			double balance = purse.getBalance();
			System.out.println("\nBalance is : " + balance);
		}
		if(info!=null)
			System.out.println(info);
	}
}
