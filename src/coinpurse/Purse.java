package coinpurse;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import coinpurse.strategy.OverWithdraw;
import coinpurse.strategy.RecursiveWithdraw;
import coinpurse.strategy.WithdrawStrategy;


/**
 *  A valuable purse contains valuables.
 *  You can insert valuables, withdraw money, check the balance,
 *  and check if the purse is full.
 *  When you withdraw money, the valuable purse decides which
 *  valuables to remove.
 *  
 *  @author Taweerat Chaiman 5710546259
 *  @version 19.02.2015
 */
public class Purse extends java.util.Observable{
    /** Collection of valuable in the purse. */
    private List<Valuable> valuable = new ArrayList<Valuable>();
    /** Capacity is maximum NUMBER of valuables the purse can hold.
     *  Capacity is set when the purse is created.
     */
    private int capacity;
    
    private WithdrawStrategy strategy;
    /** 
     *  Create a purse with a specified capacity.
     *  Add observer.
     *  @param capacity is maximum number of valuables you can put in purse.
     */
    public Purse( int capacity ) {
    	if(capacity>0)
    		this.capacity = capacity;
    	else
    		this.capacity = 0;
    }

    /**
     * Count and return the number of valuables in the purse.
     * This is the number of valuables, not their value.
     * @return the number of valuables in the purse
     */
    public int count() { 
    	return valuable.size();
    }
    
    /** 
     *  Get the total value of all items in the purse.
     *  @return the total value of items in the purse.
     */
    public double getBalance() {
    	double sum=0;
    	for(int i=0;i<valuable.size();i++){
    		sum += valuable.get(i).getValue();
    	}
    	return sum;
    }
    
    /**
     * To set withdraw behavior.
     * @param withdrawStrategy as withdraw behavior
     */
    public void setWithdrawStrategy(WithdrawStrategy withdrawStrategy){
    	this.strategy = withdrawStrategy;
    }
    
    /**
     * Return the capacity of the valuables purse.
     * @return the capacity
     */
    public int getCapacity() {
    	return capacity;
    }
    
    /** 
     *  Test whether the purse is full.
     *  The purse is full if number of items in purse equals
     *  or greater than the purse capacity.
     *  @return true if purse is full.
     */
    public boolean isFull() {
    	if(getCapacity()==0) return true;
    	return count()==getCapacity();
    }

    /** 
     * Insert a valuables into the purse.
     * The valuables is only inserted if the purse has space for it
     * and the valuables has positive value.  No worthless valuables!
     * @param valuable is a valuables object to insert into purse
     * @return true if valuables inserted, false if can't insert
     */
    public boolean insert( Valuable valuable ) {
        // if the purse is already full then can't insert anything.
    	if(!isFull() && valuable.getValue() >0){
    		this.valuable.add(valuable);
    		super.setChanged();
    		super.notifyObservers(this);
    		return true;
    	}else
    		return false;
    }
    
    /**  
     *  Withdraw the requested amount of money.
     *  Return an array of valuables withdrawn from purse,
     *  or return null if cannot withdraw the amount requested.
     *  @param amount is the amount to withdraw
     *  @return array of Valuable objects for money withdrawn, 
	 *    or null if cannot withdraw requested amount.
     */
    public Valuable[] withdraw( double amount ) {
    	if(amount > getBalance() || amount<=0) return null; 
    	Valuable[] withdrawn=null;
    	setWithdrawStrategy(new RecursiveWithdraw());
		withdrawn = strategy.withdraw(amount, valuable);
		
		/**If can't withdraw with recursive then use OverWithdraw strategy*/
		if(withdrawn==null && getBalance() >= amount){
			System.out.println("----Use OverWithdraw Strategy----");
			setWithdrawStrategy(new OverWithdraw());
			withdrawn = strategy.withdraw(amount, valuable);
		}
		if(withdrawn!=null){
			for(int i=0;i<withdrawn.length;i++){
    			valuable.remove(withdrawn[i]);
    		}
			super.setChanged();
			super.notifyObservers(this);
		}
    	return withdrawn;
    		
    		
	}
  
    /** 
     * toString returns a string description of the purse contents.
     * It can return whatever is a useful description.
     * @return String as information of purse
     */
    public String toString() {
    	String totalStr = String.format("%d valuables with value %.1f", count(),getBalance());
    	return totalStr;
    }

}
