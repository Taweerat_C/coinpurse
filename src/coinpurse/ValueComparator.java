package coinpurse;

import java.util.Comparator;
/**
 * ValueComparator class that implements Comparator. 
 * @author Taweerat Chaiman 5710546259
 * @version 10.02.2015
 */
public class ValueComparator implements Comparator<Valuable>{
	/**
	 * Compare a value of each valuable.
	 * @return negitive when a is less than b
	 * zero when a is equals to b
	 * positive when a is greater than b
	 * @param a as first valuable.
	 * @param b as second valuable.
	 */
	public int compare(Valuable a,Valuable b){
		double  diff = a.getValue()-b.getValue();
		if(diff<0)
			return -1;
		else if(diff>0)
			return 1;
		else
			return 0;
		
	}
}
