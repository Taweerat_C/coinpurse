package coinpurse;

import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 * Show purse's balance in GUI.
 * @author Taweerat Chaiman 5710546259
 * @version 24.02.2015
 */
public class GUIBalance implements java.util.Observer{
	/**Class attributes.*/
	private JFrame frame;
	private JLabel lb_balance ;
	
	/**
	 * Constuctor to call initComponent().
	 */
	public GUIBalance(){
		initComponent();
	}
	
	/**
	 * Initial components of GUI.
	 */
	public void initComponent(){
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		lb_balance = new JLabel("Balance is 0 Baht");
		Font font = lb_balance.getFont();
		lb_balance.setFont(new Font(font.getName(),Font.BOLD,35));
		frame.add(lb_balance);
		frame.setVisible(true);
		frame.pack();
	}
	
	/**
	 * Update the data when purse is updated.
	 * @param subject as Observable that listening to
	 * @param info as information
	 */
	public void update(java.util.Observable subject,Object info){
		if(subject instanceof Purse){
			Purse purse = (Purse)subject;
			double balance = purse.getBalance();
			lb_balance.setText("Balance is " + balance + " Baht");
			frame.pack();
		}
	}
}
